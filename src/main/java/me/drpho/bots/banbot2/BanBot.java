package me.drpho.bots.banbot2;

import discord4j.core.DiscordClient;
import discord4j.core.GatewayDiscordClient;
import discord4j.core.event.domain.guild.GuildCreateEvent;
import discord4j.core.event.domain.guild.MemberJoinEvent;
import discord4j.core.event.domain.lifecycle.ReadyEvent;
import discord4j.core.event.domain.message.MessageCreateEvent;
import discord4j.core.event.domain.message.ReactionAddEvent;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.User;
import discord4j.core.object.reaction.Reaction;
import me.drpho.bots.banbot2.models.Ban;
import me.drpho.bots.banbot2.models.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BanBot {
	public static final String PERMS = "469838917";
	public static final String DB_NAME = "bot.db";

	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

	public static GatewayDiscordClient CLIENT;

	public static void main (String [] args) {
		if (args.length < 1) {
			logger.error("Must have one parameter with the bot token.");
			System.exit(1);
		}
		String db = DB_NAME;
		if (args.length >= 2) {
			db = args[1];
		}

		try {
			DataManager.connect(db);
		} catch (SQLException ignored) {
			logger.error("Could not connect to SQL Database. Exiting.");
			System.exit(1);
		}
		final DiscordClient client = DiscordClient.create(args[0]);
		final GatewayDiscordClient gateway = client.login().block();

		registerEvents(gateway);
		logger.info("Setup completed.");
		CLIENT = gateway;
		gateway.onDisconnect().block();
	}

	private static void registerEvents(GatewayDiscordClient client) {
		client.getEventDispatcher().on(ReadyEvent.class)
				.subscribe(event -> {
					User self = event.getSelf();
					logger.info("Logged in as {}#{}", self.getUsername(), self.getDiscriminator());
					logger.info("\n\n\n\n\n\nUse the following link to invite the bot to your server:");
					logger.info("https://discordapp.com/oauth2/authorize?client_id=" + client.getSelfId().asString() + "&scope=bot&permissions=" + PERMS);
					logger.info("If you don't allow all the perms, the bot probably won't work right.\n\n\n\n\n");
				});
		client.getEventDispatcher().on(GuildCreateEvent.class)
				.subscribe(event -> {
					try {
						DataManager.joinServer(event.getGuild().getId().asString());
						logger.info("Joined guild {}!", event.getGuild().getName());
					} catch (SQLException ignored) {
						logger.warn("SQL exception when joining server {}!", event.getGuild().getName());
					}
				});
		client.getEventDispatcher().on(MessageCreateEvent.class)
				.filter(event -> event.getGuildId().isPresent())
				.filter(event -> event.getMember().isPresent())
				.subscribe(event -> {
					try {
						Server server = DataManager.getServer(event.getGuildId().get().asString());
						if (server == null) {
							throw new IllegalStateException("Accessing non-existent server!");
						}
						String content = event.getMessage().getContent().trim();
						if (content.toLowerCase().startsWith(server.getVotePrefix())) {
							EventHandler.processBanMessage(server, content, event.getMessage().getId().asString(),
									event.getMessage().getTimestamp(), event.getMessage().getChannelId().asString(),
									event.getMember().get().getId().asString());
						} else if (content.toLowerCase().startsWith(server.getStatsPrefix())) {
							EventHandler.processStatsMessage(server, content, event.getMessage().getId().asString(),
									event.getMessage().getChannelId().asString());
						}
					} catch (SQLException ignored) {
						logger.warn("SQL exception when processing message {}!", event.getMessage().getId().asString());
					} catch (IllegalStateException e) {
						logger.warn("Could not process message: {}", e.getMessage());
					}
				});
		client.getEventDispatcher().on(ReactionAddEvent.class)
				.filter(event -> event.getGuildId().isPresent())
				.subscribe(event -> {
					try {
						Server server = DataManager.getServer(event.getGuildId().get().asString());
						if (server == null) {
							throw new IllegalStateException("Accessing non-existent server!");
						}
						Ban ban = DataManager.getBanByVoteMessageId(event.getMessageId().asString());
						if (ban == null || !ban.getBanType().isProcessVote()
							|| !EventHandler.getFormattedEmojiString(event.getEmoji()).equals(server.getEmoji())) {
							return;
						}
						Optional<Integer> numReacts = event.getMessage().map(message -> message.getReactions().stream()
										.filter(reaction -> reaction.getEmoji().equals(event.getEmoji())).map(Reaction::getCount).findFirst())
										.block();
						if (!numReacts.isPresent()) {
							throw new IllegalStateException("Could not get the number of reactions - really odd.");
						}
						logger.info("Vote for {} now has {} votes.", ban.getTargetId(), numReacts.get());
						EventHandler.processReaction(server, ban, numReacts.get(), event.getGuild().map(Guild::getName));
					} catch (SQLException ignored) {
						logger.warn("SQL exception when processing reaction {}!", event.getMessageId().asString());
					} catch (IllegalStateException e) {
						logger.warn("Could not process reaction: {}", e.getMessage());
					}
				});
		client.getEventDispatcher().on(MemberJoinEvent.class)
				.subscribe(event -> {
					try {
						Server server = DataManager.getServer(event.getGuildId().asString());
						if (server == null) {
							throw new IllegalStateException("Accessing non-existent server!");
						}
						List<Ban> banList = DataManager.getBansByUser(event.getGuildId().asString(), event.getMember().getId().asString());
						banList = banList.stream().filter(ban -> ban.getBanType().isProcessJoin()).collect(Collectors.toList());
						if (banList.isEmpty()) {
							logger.debug("Irrelevant join event.");
							return;
						}
						if (banList.size() > 1) {
							throw new IllegalStateException("Multiple ban objects to choose from!");
						}
						EventHandler.processJoin(server, banList.get(0), event.getMember());
					} catch (SQLException ignored) {
						logger.warn("SQL exception when processing join for {}!", event.getMember().getUsername());
					} catch (IllegalStateException e) {
						logger.warn("Could not process join event: {}", e.getMessage());
					}
				});
	}
}
