package me.drpho.bots.banbot2;

import me.drpho.bots.banbot2.models.Ban;
import me.drpho.bots.banbot2.models.BanType;
import me.drpho.bots.banbot2.models.Server;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.invoke.MethodHandles;
import java.sql.*;
import java.time.Instant;
import java.util.*;

public class DataManager {
	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

	private static Connection conn;

	/**
	 * Connect to the database
	 * @param fileName the database file name
	 */
	public static void connect(String fileName) throws SQLException {
		File f = new File(fileName);
		String url = "jdbc:sqlite:" + f.getAbsolutePath();
		try {
			conn = DriverManager.getConnection(url);
			logger.debug("Database driver: {}", conn.getMetaData().getDriverName());
			logger.info("Database connection established.");
			createTables();

			new Timer().scheduleAtFixedRate(new TimerTask() {
				@Override
				public void run() {
					try {
						String selectServer = "SELECT * "
								+ "FROM servers;";
						String selectPendingBans = "SELECT * "
								+ "FROM bans WHERE create_time < ? AND ban_type = ?;";
						String selectExecutedBans = "SELECT * "
								+ "FROM bans WHERE ban_time < ? AND ban_type = ?;";
						Statement stmt = conn.createStatement();
						// create a new table
						stmt.execute(selectServer);
						ResultSet rs = stmt.getResultSet();
						while (rs.next()) {
							Server server = serverFromResult(rs);
							//logger.debug(String.valueOf(server));
							int expireBefore = (int) (Instant.now().getEpochSecond() - server.getVoteExpire()*60);
							int unbanBefore = (int) (Instant.now().getEpochSecond() - server.getBanTimeout()*60);
							PreparedStatement pstmt = conn.prepareStatement(selectPendingBans);
							pstmt.setInt(1, expireBefore);
							pstmt.setString(2, BanType.PENDING.name());
							ResultSet rsBan = pstmt.executeQuery();
							while (rsBan.next()) {
								EventHandler.processExpire(server, banFromResult(rsBan));
							}
							pstmt = conn.prepareStatement(selectExecutedBans);
							pstmt.setInt(1, unbanBefore);
							pstmt.setString(2, BanType.EXECUTED.name());
							rsBan = pstmt.executeQuery();
							while (rsBan.next()) {
								EventHandler.processUnban(server, banFromResult(rsBan));
							}
						}
					} catch (SQLException | IllegalArgumentException e) {
						logger.warn("Could not process timeout! - {}", e.getMessage());
					}
				}
			}, 10000, 10000);
		} catch (SQLException e) {
			logger.error("SQL Connect Error: {}", e.getMessage());
			throw e;
		}
	}

	public static void joinServer(String guildId) throws SQLException {
		String insertSQL = "INSERT INTO servers(guild_id, first_seen) VALUES(?, ?);";

		if (getServer(guildId) == null) {
			try {
				PreparedStatement insertServer = conn.prepareStatement(insertSQL);
				insertServer.setString(1, guildId);
				insertServer.setLong(2, Instant.now().getEpochSecond());
				insertServer.executeUpdate();
			} catch (SQLException e) {
				logger.error("SQL Insert Error: {}", e.getMessage());
				throw e;
			}
		}
	}

	@Nullable
	public static Server getServer(String guildId) throws SQLException {
		String sql = "SELECT * "
				+ "FROM servers WHERE guild_id = ?;";

		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, guildId);
			ResultSet rs  = pstmt.executeQuery();

			if (rs.next()) {
				return serverFromResult(rs);
			} else {
				return null;
			}
		} catch (SQLException e) {
			logger.error("SQL Query Error: {}", e.getMessage());
			throw e;
		}
	}

	@Nullable
	public static Ban getBanByVoteMessageId(String voteMessageId) throws SQLException {
		String sql = "SELECT * "
				+ "FROM bans WHERE vote_message_id = ?;";
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, voteMessageId);
			List<Ban> bans = getBans(pstmt);
			return bans.isEmpty() ? null : bans.get(0);
		} catch (SQLException e) {
			logger.error("General SQL error: {}", e.getMessage());
			throw e;
		}
	}

	public static List<Ban> getBansByUser(String serverId, String targetId) throws SQLException {
		String sql = "SELECT * "
				+ "FROM bans WHERE target_id = ? AND server_id = ?;";
		try {
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, targetId);
			pstmt.setString(2, serverId);
			return getBans(pstmt);
		} catch (SQLException e) {
			logger.error("General SQL error: {}", e.getMessage());
			throw e;
		}
	}

	private static List<Ban> getBans(PreparedStatement pstmt) throws SQLException {
		try {
			ResultSet rs = pstmt.executeQuery();
			List<Ban> bans = new ArrayList<>();
			while (rs.next()) {
				bans.add(banFromResult(rs));
			}
			return bans;
		} catch (SQLException e) {
			logger.error("SQL Query Error: {}", e.getMessage());
			throw e;
		} catch (IllegalArgumentException ignored) {
			logger.error("SQL Parse Error: Unrecognized ban type!");
			throw new SQLException();
		}
	}

	public static void putBan(Ban ban) throws SQLException {
		try {
			if (ban.getId() == null) {
				String insertBan = "INSERT INTO bans (id, server_id, create_time, create_message_id, author_id, ban_time, " +
						"vote_message_id, rejoin_time, channel_id, target_id, votes, reason, nickname, roles, ban_type)\n" +
						"VALUES (null, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
				PreparedStatement pstmt = conn.prepareStatement(insertBan);
				updatePreparedStatement(pstmt, ban);
				pstmt.executeUpdate();

			} else {
				String updateBan = "UPDATE bans SET server_id = ? , create_time = ? , create_message_id = ? , author_id = ? , ban_time = ? , " +
						"vote_message_id = ? , rejoin_time = ? , channel_id = ? , target_id = ? , votes = ? , reason = ? , nickname = ? , roles = ? , ban_type = ? \n" +
						"WHERE ID = ?;";
				PreparedStatement pstmt = conn.prepareStatement(updateBan);
				updatePreparedStatement(pstmt, ban);
				pstmt.setInt(15, ban.getId());
				pstmt.executeUpdate();
			}
		} catch (SQLException e) {
			logger.error("SQL Insert error: {}", e.getMessage());
			throw e;
		}

	}

	private static void updatePreparedStatement(PreparedStatement pstmt, Ban ban) throws SQLException {
		pstmt.setString(1, ban.getServerId());
		pstmt.setLong(2, ban.getCreateTime());
		pstmt.setString(3, ban.getCreateMessageId());
		pstmt.setString(4, ban.getAuthorId());
		pstmt.setObject(5, ban.getBanTime(), Types.INTEGER);
		pstmt.setObject(6, ban.getVoteMessageId(), Types.VARCHAR);
		pstmt.setObject(7, ban.getRejoinTime(), Types.INTEGER);
		pstmt.setString(8, ban.getChannelId());
		pstmt.setString(9, ban.getTargetId());
		pstmt.setObject(10, ban.getVotes(), Types.INTEGER);
		pstmt.setObject(11, ban.getReason(), Types.VARCHAR);
		pstmt.setObject(12, ban.getNickname(), Types.VARCHAR);
		JSONArray roles = new JSONArray();
		if (ban.getRoles() != null) {
			for(String roleId : ban.getRoles()) {
				roles.put(roleId);
			}
		}
		pstmt.setObject(13, roles.toString(), Types.VARCHAR);
		pstmt.setString(14, ban.getBanType().name());
	}

	private static void createTables() throws SQLException {
		String createBans = "CREATE TABLE IF NOT EXISTS bans (\n"
				+ "	id INTEGER PRIMARY KEY,\n"
				+ "	server_id TEXT NOT NULL,\n"
				+ " create_time INT NOT NULL,\n"
				+ "	create_message_id TEXT UNIQUE NOT NULL,\n"
				+ " author_id TEXT NOT NULL,\n"
				+ " ban_time INT,\n"
				+ " vote_message_id TEXT UNIQUE,\n"
				+ " rejoin_time INT,\n"
				+ " channel_id TEXT NOT NULL,\n"
				+ " target_id TEXT NOT NULL,\n"
				+ " votes INT DEFAULT 0,\n"
				+ " reason TEXT, \n"
				+ " nickname TEXT,\n"
				+ " roles TEXT,\n"
				+ " ban_type TEXT NOT NULL\n"
				+ ");";
		String createServers = "CREATE TABLE IF NOT EXISTS servers (\n"
				+ "	id INTEGER PRIMARY KEY,\n"
				+ "	guild_id TEXT UNIQUE NOT NULL,\n"
				+ " first_seen INT NOT NULL,\n"
				+ " emoji TEXT,\n"
				+ " vote_prefix TEXT,\n"
				+ " stats_prefix TEXT,\n"
				+ " lang TEXT,\n"
				+ " votes_needed INT DEFAULT -1,\n"
				+ " ban_timeout INT DEFAULT -1,\n"
				+ " vote_expire INT DEFAULT -1\n"
				+ ");";

		Statement stmt = conn.createStatement();
		// create a new table
		stmt.execute(createBans);
		stmt.execute(createServers);
	}

	public static void main (String[] args) throws SQLException {
		connect("bot.db");
	}

	private static Ban banFromResult(ResultSet rs) throws SQLException, IllegalArgumentException {
		JSONArray roles = new JSONArray(rs.getString("roles"));
		Set<String> roleSet = new HashSet<>();
		for (int i = 0; i < roles.length(); i++) {
			roleSet.add(roles.getString(i));
		}
		return new Ban(rs.getInt("id"), rs.getString("server_id"), rs.getString("create_message_id"),
				rs.getString("vote_message_id"), rs.getString("channel_id"), rs.getString("author_id"),
				rs.getString("target_id"), rs.getString("reason"), rs.getString("nickname"), roleSet,
				rs.getInt("votes"), rs.getInt("create_time"), rs.getLong("ban_time"), rs.getLong("rejoin_time"),
				Enum.valueOf(BanType.class, rs.getString("ban_type")));
	}

	private static Server serverFromResult(ResultSet rs) throws SQLException, IllegalArgumentException {
		return new Server(rs.getInt("id"), rs.getString("guild_id"), rs.getLong("first_seen"),
				rs.getString("emoji"), rs.getString("vote_prefix"),
				rs.getString("stats_prefix"), rs.getString("lang"), rs.getInt("votes_needed"),
				rs.getInt("ban_timeout"), rs.getInt("vote_expire"));
	}

	public static void changeState(int id, BanType type) throws SQLException {
		try {
			String sql = "UPDATE bans\n" +
					"SET ban_type = ?\n" +
					"WHERE id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, type.name());
			pstmt.setInt(2, id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL Update state error: {}", e.getMessage());
			throw e;
		}

	}

	public static void updateVotes(Integer id, int numReacts) throws SQLException {
		try {
			String sql = "UPDATE bans\n" +
					"SET votes = ?\n" +
					"WHERE id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, numReacts);
			pstmt.setInt(2, id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL Update votes error: {}", e.getMessage());
			throw e;
		}
	}

	public static void setRejoinTime(Integer id, Instant rejoinTime) throws SQLException {
		try {
			String sql = "UPDATE bans\n" +
					"SET rejoin_time = ?\n" +
					"WHERE id = ?";
			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, ((int) rejoinTime.getEpochSecond()));
			pstmt.setInt(2, id);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL Update time error: {}", e.getMessage());
			throw e;
		}
	}
}
