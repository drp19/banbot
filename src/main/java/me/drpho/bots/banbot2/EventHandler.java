package me.drpho.bots.banbot2;

import discord4j.common.util.Snowflake;
import discord4j.core.object.entity.Guild;
import discord4j.core.object.entity.Member;
import discord4j.core.object.entity.Message;
import discord4j.core.object.entity.channel.GuildMessageChannel;
import discord4j.core.object.reaction.ReactionEmoji;
import discord4j.rest.http.client.ClientException;
import me.drpho.bots.banbot2.models.Ban;
import me.drpho.bots.banbot2.models.BanType;
import me.drpho.bots.banbot2.models.Server;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.lang.invoke.MethodHandles;
import java.sql.SQLException;
import java.time.Instant;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class EventHandler {

	private static final Logger logger = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass().getSimpleName());

	private static final Pattern CMD_PATTERN = Pattern.compile("^.* +<@!?(\\d{18})>(?> +(.*)|)$");

	/**
	 * Processes a ban message in the server - will match the command prefix, but not necessarily the content format.
	 * @param server Server object associated with the guild
	 * @param content Content of the message
	 * @param messageId ID of the original message
	 * @param messageTime Time of the original message
	 * @param channelId Channel the voteban message is in
	 * @param authorId Author of the voteban message
	 * @throws SQLException Exception from the database
	 * @throws IllegalStateException Used to signal that the database is in a weird state
	 */
	public static void processBanMessage(@NotNull Server server, String content, String messageId, Instant messageTime, String channelId, String authorId) throws SQLException, IllegalStateException {
		ResourceBundle rb = ResourceBundle.getBundle("me.drpho.bots.banbot2.constants",new Locale(server.getLang()));
		Mono<GuildMessageChannel> channelObj = BanBot.CLIENT.getChannelById(Snowflake.of(channelId)).map(channel -> (GuildMessageChannel) channel);

		Matcher matcher = CMD_PATTERN.matcher(content);
		if (!matcher.matches()) {
			logger.debug("Non matching ban message: {}", content);
			return;
		}
		logger.info("New valid ban message: {}", content);

		String userId = matcher.group(1), reason = matcher.group(2);

		// if trying to ban banbot, reject and send a message. (non-blocking)
		if (userId.equals(BanBot.CLIENT.getSelfId().asString())) {
			logger.info("Self-ban. Ignoring.");
			DataManager.putBan(Ban.createIncomplete(server.getGuildId(), messageId, channelId, authorId, userId, reason, messageTime, BanType.INVALID));
			channelObj.flatMap(channel -> channel.createMessage(rb.getString("ban_self"))).subscribe();
			return;
		}

		// if there is already a ban attempt for that user (or if they've already been banned), reject and send a message (non-blocking)
		// may throw exception, this is bad, but just warn and pass it on.
		long numBlockers = DataManager.getBansByUser(server.getGuildId(), userId).stream().filter(ban -> ban.getBanType().isBlocking()).count();
		if (numBlockers >= 2) {
			throw new IllegalStateException(String.format("Multiple blocking bans for user %s", userId));
		}
		if (numBlockers == 1) {
			logger.info("Duplicate ban. Ignoring.");
			DataManager.putBan(Ban.createIncomplete(server.getGuildId(), messageId, channelId, authorId, userId, reason, messageTime, BanType.INVALID));
			channelObj.flatMap(channel -> channel.createMessage(rb.getString("existing_ban"))).subscribe();
			return;
		}

		// make sure the user is in this server and get their mention (blocking, since otherwise error handling this gets annoying)
		String mention;
		try {
			mention = BanBot.CLIENT.getMemberById(Snowflake.of(server.getGuildId()), Snowflake.of(userId))
					.map(Member::getMention).block();
		} catch (ClientException ignored) {
			logger.info("Error retrieving mentioned user - they probably aren't in this server. Ignoring");
			DataManager.putBan(Ban.createIncomplete(server.getGuildId(), messageId, channelId, authorId, userId, reason, messageTime, BanType.INVALID));
			return;
		}

		// asynchronously get both the emoji and the channel (this usually isn't gonna take very long but its good practice)
		Mono<Tuple2<GuildMessageChannel, ReactionEmoji>> async = channelObj.zipWith(getEmoji(server.getGuildId(), server.getEmoji()));

		// send the message and perform SQL actions accordingly (blocking since error handling is important here too)
		try {
			Message message = async.flatMap(tuple -> tuple.getT1()
					.createMessage(createVoteMessageContent(rb, tuple.getT2(), reason, mention, server.getBanTimeout(), server.getVotesNeeded()))
					.doOnNext(mes -> mes.addReaction(tuple.getT2())
									.doOnError(error -> logger.warn("Could not add reaction to message. Something weird happened - {}", error.getMessage()))
									.subscribe()))
					.block();
			logger.info("Starting ban vote for {}", userId);
			DataManager.putBan(Ban.createPending(server.getGuildId(), messageId, message.getId().asString(), channelId, authorId, userId, reason, messageTime, BanType.PENDING));
		} catch (ClientException e) {
			logger.warn("Could not create vote message - {}. Deleting ban.", e.getMessage());
			DataManager.putBan(Ban.createIncomplete(server.getGuildId(), messageId, channelId, authorId, userId, reason, messageTime, BanType.FAILED));
		}
	}

	public static void processReaction(Server server, Ban ban, long numReacts, Mono<String> guildName) throws SQLException {
		DataManager.updateVotes(ban.getId(), (int) numReacts);
		if (ban.getBanType() == BanType.PENDING && numReacts >= server.getVotesNeeded() + 1) {
			ban(server, ban, guildName);
		}

	}

	private static String createVoteMessageContent(ResourceBundle rb, ReactionEmoji emoji, String reason, String mention, int banTimeout, int votesNeeded) {
		String toSend;
		if (reason == null) {
			toSend = String.format(rb.getString("react_no_reason"), getEmojiString(emoji, false), mention, banTimeout, votesNeeded + 1);
		} else {
			toSend = String.format(rb.getString("react_reason"), getEmojiString(emoji, false), mention, banTimeout, reason, votesNeeded + 1);
		}
		return toSend;
	}

	public static void processStatsMessage(@NotNull Server server, String content, String messageId, String channelId) throws SQLException {
		//TODO
	}

	public static void ban(Server server, Ban ban, Mono<String> guildName) throws SQLException {
		Member mem;
		ResourceBundle rb = ResourceBundle.getBundle("me.drpho.bots.banbot2.constants",new Locale(server.getLang()));
		Mono<GuildMessageChannel> channelObj = BanBot.CLIENT.getChannelById(Snowflake.of(ban.getChannelId())).map(channel -> (GuildMessageChannel) channel);

		// make sure the user is in this server (and can ban) and get their member object (blocking, since it gets used everywhere and errors are annoying)
		try {
			Tuple2<Boolean, Member> async = BanBot.CLIENT.getMemberById(Snowflake.of(server.getGuildId()), Snowflake.of(ban.getTargetId()))
					.flatMap(member -> member.isHigher(BanBot.CLIENT.getSelfId()).zipWith(Mono.just(member)))
					.block();
			if (async.getT1()) {
				logger.warn("Target member is a higher permission than me! Can't ban.");
				DataManager.changeState(ban.getId(), BanType.INVALID);
				channelObj.flatMap(channel ->
						channel.createMessage(String.format(rb.getString("ban_err"), async.getT2().getNickname().orElse(async.getT2().getUsername())))).subscribe();
				return;
			}
			mem = async.getT2();
		} catch (ClientException ignored) {
			logger.info("Error retrieving mentioned user - they probably aren't in the server anymore.");
			DataManager.changeState(ban.getId(), BanType.FAILED);
			return;
		}

		// collect variables
		Set<String> roleIds = mem.getRoleIds().stream().map(Snowflake::asString).collect(Collectors.toSet());
		String nickname = mem.getNickname().orElse(null);
		String banName = nickname != null ? nickname : mem.getUsername();
		String reason = ban.getReason() == null ? "" : ban.getReason();

		// send PM and ban - if PM fails, continue with ban. Ban is blocking for error handling/
		logger.info("Attempting to ban {}", banName);
		Instant banTime = Instant.now();
		try {
			DataManager.putBan(Ban.createExecuted(ban, nickname, roleIds, banTime));
			mem.getPrivateChannel().zipWith(channelObj.flatMap(channel -> channel.createInvite(inviteCreateSpec -> {
				inviteCreateSpec.setReason("BanBot");
				inviteCreateSpec.setUnique(true);
				inviteCreateSpec.setMaxUses(1);
			})).zipWith(guildName)).flatMap(tuple ->
					tuple.getT1().createMessage(String.format(
							rb.getString(ban.getReason() == null ? "priv_message_no_reason" : "priv_message"),
							tuple.getT2().getT2(), tuple.getT2().getT1().getCode(), reason)))
					.doOnError(e -> {
						logger.warn("General error in creating PM for {}. This is fine?", ban.getTargetId());
						logger.warn(e.getMessage());
						channelObj.flatMap(channel -> channel.createMessage(String.format(rb.getString("priv_message_err"), banName)))
								.doOnError(error -> logger.warn("Failed to send PM warning message. Unlucky. - {}", error.getMessage()))
								.onErrorStop().subscribe();
					}).onErrorResume(e -> Mono.empty()).block(); // all errors before this don't matter for the functionality
			BanBot.CLIENT.getGuildById(Snowflake.of(server.getGuildId()))
			.flatMap(guild -> guild.ban(mem.getId(), banQuerySpec -> banQuerySpec.setReason("VOTEBAN: " + reason))).block();
		} catch (ClientException e) {
			logger.warn("Could not ban user {} - {}", ban.getTargetId(), e.getMessage());
			channelObj.flatMap(channel -> channel.createMessage(String.format(rb.getString("ban_err"), banName)))
					.doOnError(error -> logger.warn("Failed to send ban failure warning message. Unlucky. - {}", error.getMessage()))
					.onErrorStop().subscribe();
			DataManager.changeState(ban.getId(), BanType.FAILED);
			return;
		}

		// modify original message (non-blocking), set timeout, and send success message (non-blocking)
		logger.info("Banned {}, starting timer.", banName);
		BanBot.CLIENT.getMessageById(Snowflake.of(server.getGuildId()), Snowflake.of(ban.getVoteMessageId()))
				.flatMap(message -> message.edit(mes -> mes.setContent(message.getContent() + " (" + rb.getString("done") + ")")))
				.doOnError(error -> logger.warn("Could not find/edit original message with (DONE).")).onErrorStop().subscribe();
		String toSend;
		if (reason.equals("")) {
			toSend = String.format(rb.getString("ban_success_no_reason"),banName,server.getBanTimeout());
		} else {
			toSend = String.format(rb.getString("ban_success_reason"),banName,server.getBanTimeout(),reason);
		}
		channelObj.flatMap(channel -> channel.createMessage(toSend)).doOnError(e -> logger.warn("Could not send ban success message - {}", e.getMessage())).subscribe();
	}

	public static void processJoin(@NotNull Server server, Ban ban, @NotNull Member member) throws SQLException {
		ResourceBundle rb = ResourceBundle.getBundle("me.drpho.bots.banbot2.constants",new Locale(server.getLang()));
		Set<Snowflake> roleList = ban.getRoles().stream().map(Snowflake::of).collect(Collectors.toSet());
		DataManager.changeState(ban.getId(), BanType.FINISHED);
		DataManager.setRejoinTime(ban.getId(), Instant.now());
		// edit member
		BanBot.CLIENT.getGuildById(Snowflake.of(server.getGuildId())).map(Guild::getRoleIds)
			.flatMap(guildRoles -> member.edit(guildMemberEditSpec -> {
				if (ban.getNickname() != null)
					guildMemberEditSpec.setNickname(ban.getNickname().substring(0, Math.min(ban.getNickname().length(), 32)));
				guildMemberEditSpec.setRoles(roleList.stream().filter(guildRoles::contains).collect(Collectors.toSet()));
		})).doOnError(error -> {
			logger.warn("Not enough permissions to process member add. - {}", error.getMessage());
			BanBot.CLIENT.getChannelById(Snowflake.of(ban.getChannelId())).map(channel -> (GuildMessageChannel) channel)
					.flatMap(channel -> channel.createMessage(String.format(rb.getString("add_err"), member.getDisplayName()))).subscribe();
		}).subscribe(Void -> logger.info("Banned user {} rejoin processed.", ban.getTargetId()));
	}

	public static void processExpire(Server server, Ban ban) throws SQLException {
		ResourceBundle rb = ResourceBundle.getBundle("me.drpho.bots.banbot2.constants",new Locale(server.getLang()));
		logger.info("Vote timer for {} expired.", ban.getTargetId());

		DataManager.changeState(ban.getId(), BanType.EXPIRED);
		BanBot.CLIENT.getMessageById(Snowflake.of(server.getGuildId()), Snowflake.of(ban.getVoteMessageId()))
				.flatMap(message -> message.edit(mes -> mes.setContent(message.getContent() + " (" + rb.getString("expired") + ")")))
				.doOnError(error -> logger.warn("Could not find/edit original message with (EXPIRED).")).onErrorStop().subscribe();
	}

	public static void processUnban(Server server, Ban ban) throws SQLException {
		ResourceBundle rb = ResourceBundle.getBundle("me.drpho.bots.banbot2.constants",new Locale(server.getLang()));
		logger.info("Ban for {} ended.", ban.getTargetId());

		DataManager.changeState(ban.getId(), BanType.ENDED);
		BanBot.CLIENT.getGuildById(Snowflake.of(server.getGuildId())).flatMap(guild -> guild.unban(Snowflake.of(ban.getTargetId())))
				.doOnError(error -> logger.warn("Could not unban {}! - {}", ban.getTargetId(), error.getMessage())).onErrorStop().subscribe();
	}

	private static void printStats(String channelId) {
//		Map<String, Integer> leaderboard = db.getLeaderboard();
//		if (leaderboard.size() == 0) {
//			try {
//				sendMessage(channelId, rb.getString("lb_no_leaderboard"));
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			return;
//		}
//		try {
//			StringBuilder names = new StringBuilder(), bans = new StringBuilder();
//			for (Map.Entry<String, Integer> person : leaderboard.entrySet()) {
//				Member m = getMember(person.getKey());
//				if (m == null) {
//					User u = guildObj.getClient().getUserById(Snowflake.of(person.getKey())).block();
//					names.append(u.getUsername()).append("#").append(u.getDiscriminator()).append("\n");
//				} else {
//					names.append(m.getDisplayName()).append("\n");
//				}
//				bans.append(person.getValue()).append("\n");
//			}
//			TextChannel channelObj = getChannel(channelId);
//			channelObj.createMessage(mspec -> mspec.setEmbed(spec -> {
//				spec.setAuthor(rb.getString("name"), null, null);
//				spec.setTitle(rb.getString("lb_title"));
//				spec.setDescription(rb.getString("lb_desc"));
//				spec.addField(rb.getString("lb_member"), names.toString(), true);
//				spec.addField(rb.getString("lb_bans"), bans.toString(), true);
//				spec.setFooter(rb.getString("author"), null);
//			})).block();
//		} catch (Exception e) {
//			logger.warn("Could not send message in channel " + channelId);
//		}

	}

	private static Mono<ReactionEmoji> getEmoji(String guildId, String id) {
		Snowflake snow;
		if (id.length() < 2) {
			logger.warn("Emoji string {} is not processable - id too short. Using default.", id);
			return Mono.just(ReactionEmoji.unicode(Server.DEFUALT_EMOJI));
		} else {
			switch (id.charAt(0)) {
				case 'c': {
					snow = Snowflake.of(id.substring(1));
					return BanBot.CLIENT.getGuildEmojiById(Snowflake.of(guildId), snow).map(ReactionEmoji::custom).map(emoji -> (ReactionEmoji)emoji)
							.doOnError(error -> logger.warn("Emoji string {} is not processable -  Could not parse custom ID. Using default.", id))
							.onErrorReturn(ReactionEmoji.unicode(Server.DEFUALT_EMOJI));
				}
				case 'u': {
					return Mono.just(ReactionEmoji.unicode(id.substring(1)));
				}
				default: {
					logger.warn("Emoji string {} is not processable - must start with c or u. Using default.", id);
					return Mono.just(ReactionEmoji.unicode(Server.DEFUALT_EMOJI));
				}
			}
		}
	}

	private static String getEmojiString(ReactionEmoji emojiObj, boolean toSQLFormat) {
		if (emojiObj instanceof ReactionEmoji.Unicode) {
			if (toSQLFormat) {
				return "u" + ((ReactionEmoji.Unicode) emojiObj).getRaw();
			}
			return ((ReactionEmoji.Unicode) emojiObj).getRaw();
		} else {
			ReactionEmoji.Custom customObj = (ReactionEmoji.Custom)emojiObj;
			if (toSQLFormat) {
				return "c" + customObj.getId().asString();
			}
			return "<:" + customObj.getName() + ":" + customObj.getId().asString() + ">";
		}
	}

	public static String getFormattedEmojiString(ReactionEmoji emojiObj) {
		return getEmojiString(emojiObj, true);
	}

}
