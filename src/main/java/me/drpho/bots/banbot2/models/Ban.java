package me.drpho.bots.banbot2.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.Instant;
import java.util.Set;

@AllArgsConstructor()
@Getter
public final class Ban {
	private final Integer id;
	private final String serverId, createMessageId, voteMessageId, channelId, authorId, targetId, reason, nickname;
	private final Set<String> roles;
	private final Integer votes;
	private final long createTime;
	private final Long banTime, rejoinTime;
	private final BanType banType;


	public static Ban createIncomplete(String guildId, String createMessageId, String channelId, String authorId, String targetId, String reason, Instant createTime, BanType type) {
		return new Ban(null, guildId, createMessageId, null, channelId, authorId, targetId, reason, null, null,
				null, createTime.getEpochSecond(), null, null, type);
	}

	public static Ban createPending(String guildId, String createMessageId, String voteMessageId, String channelId, String authorId, String targetId, String reason, Instant createTime, BanType type) {
		return new Ban(null, guildId, createMessageId, voteMessageId, channelId, authorId, targetId, reason, null, null,
				null, createTime.getEpochSecond(), null, null, type);
	}

	public static Ban createExecuted(Ban b, String nickname, Set<String> roles, Instant banTime) {
		return new Ban(b.id, b.serverId, b.createMessageId, b.voteMessageId, b.channelId, b.authorId, b.targetId, b.reason, nickname, roles,
				b.votes, b.createTime, banTime.getEpochSecond(), null, BanType.EXECUTED);
	}


	}
