package me.drpho.bots.banbot2.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum BanType {
	PENDING(true, false, true),
	EXECUTED(true, true, true),
	ENDED(false, true, true),
	FINISHED(false, false, false),
	EXPIRED(false, false, false),
	INVALID(false, false, false),
	FAILED(false, false, false);

	private final boolean processVote, processJoin, isBlocking;
}
