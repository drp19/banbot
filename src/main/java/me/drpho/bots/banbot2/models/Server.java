package me.drpho.bots.banbot2.models;

import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

@Getter
@ToString
public final class Server {
	private final String guildId, emoji, votePrefix, statsPrefix, lang;
	private final int votesNeeded, banTimeout, voteExpire, id;
	private final long firstSeen;

	public static final String DEFUALT_EMOJI = "\uD83D\uDD28";

	public Server(int id, @NonNull String guildId, long firstSeen, String emoji, String votePrefix, String statsPrefix, String lang, int votesNeeded, int banTimeout, int voteExpire) {
		this.id = id;
		this.guildId = guildId;
		this.firstSeen = firstSeen;
		this.emoji = emoji != null ? emoji : "u"+DEFUALT_EMOJI;
		this.votePrefix = votePrefix != null ? votePrefix : "/voteban";
		this.statsPrefix = statsPrefix != null ? statsPrefix : "/votestats";
		this.lang = lang != null ? lang : "en";
		this.votesNeeded = votesNeeded != -1 ? votesNeeded : 3;
		this.banTimeout = banTimeout != -1 ? banTimeout : 5;
		this.voteExpire = voteExpire != -1 ? voteExpire : 15;
	}
}
